package com.bap;


import java.util.ArrayList;

import static com.bap.AllConstant.*;

/**
 * Created by bap on 7/17/2017.
 */
public class SelectiveXMLGraph {

    private ArrayList<String> arrayList;
    private ArrayList<String> arrayListRead;

    public ArrayList<String> getArrayListRead() {
        return arrayListRead;
    }

    public void initialize() {
        PathFinder pathFinder = new PathFinder(SELECTIVE_PATTERN);
        arrayList = pathFinder.findXMLPath(SELECTIVE_DIRECTORY);
        ReadXMLFile readXMLFile = new ReadXMLFile(arrayList, SELECTIVE_EXT, SELECTIVE_NAME);
        readXMLFile.read();
        arrayListRead = readXMLFile.getArrayListReturn();
    }
}
