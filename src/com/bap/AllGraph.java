package com.bap;

import java.util.ArrayList;
import java.util.HashMap;

import static com.bap.AllConstant.*;

/**
 * Created by bap on 7/17/2017.
 */
public class AllGraph {
    private ArrayList<String> arrayList;
    private HashMap hashMapRead = new HashMap();
    private ArrayList<String> allExtList;

    public HashMap getHashMapRead() {
        return hashMapRead;
    }

    public ArrayList<String> getAllExtList() {
        return allExtList;
    }

    public void initialize() {
        PathFinder pathFinder = new PathFinder(ALL_PATTERN);
        arrayList = pathFinder.findAllXMLPaths(ALL_DIRECTORY);
        ReadXMLFile readXMLFile = new ReadXMLFile(arrayList, ALL_EXT, ALL_NAME);
        readXMLFile.read();
        hashMapRead = readXMLFile.getHashMap();
        allExtList = new ArrayList<String>(hashMapRead.keySet());

    }
}
