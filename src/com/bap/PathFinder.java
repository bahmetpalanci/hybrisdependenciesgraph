package com.bap;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;


/**
 * Created by bap on 7/15/2017.
 */
public class PathFinder {

    private ArrayList<String> arrayList = new ArrayList<>();
    private String pattern;

    public PathFinder(String pattern) {
        this.pattern = pattern;
    }


    public ArrayList<String> findAllXMLPaths(String directoryName) {
        File directory = new File(directoryName); // --> get all the files from a directory

        File[] fList = directory.listFiles();

        for (File file : fList) {
            if (file.isDirectory()) {

                findXMLPath(file.getAbsolutePath());
                findAllXMLPaths(file.getAbsolutePath());

            }
        }
        return arrayList;
    }

    public ArrayList<String> findXMLPath(String str) {
        Path dir = Paths.get(str);
        try (DirectoryStream<Path> stream =
                     Files.newDirectoryStream(dir, pattern)) {
            for (Path entry : stream) {
                String path = entry.getParent() + "\\" + entry.getFileName();
//                System.out.println(path);
                arrayList.add(path);

            }
        } catch (IOException x) {
            x.printStackTrace();
        }
        return arrayList;
    }


}



