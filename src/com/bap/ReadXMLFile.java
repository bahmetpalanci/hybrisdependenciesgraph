package com.bap; /**
 * Created by bap on 7/15/2017.
 */

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import static com.bap.AllConstant.ALL_EXT;

public class ReadXMLFile {

    private ArrayList<String> arrayList;
    private HashMap<String, ArrayList> hashMap = new HashMap<>();
    private ArrayList<String> arrayListReturn = new ArrayList<String>();
    private String tag;
    private String name;
    public ReadXMLFile(ArrayList<String> arrayList, String tag, String name) {
        this.arrayList = arrayList;
        this.tag = tag;
        this.name = name;
    }

    public HashMap<String, ArrayList> getHashMap() {
        return hashMap;
    }

    public ArrayList<String> getArrayListReturn() {
        return arrayListReturn;
    }

    public void read() {
        for (int i = 0; i < arrayList.size(); i++) {
            try {
                File fXmlFile = new File(arrayList.get(i));
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(fXmlFile);
                doc.getDocumentElement().normalize();

                NodeList nList = doc.getElementsByTagName(tag);

                for (int temp = 0; temp < nList.getLength(); temp++) {

                    Node nNode = nList.item(temp);

                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                        Element eElement = (Element) nNode;
                        String attribute = eElement.getAttribute(name);
                        String baseURI = nNode.getBaseURI();
                        if (tag.equals(ALL_EXT)) { // bütün graph mı yoksa seçtiğimiz bir graph mı kontrolü
                            format(baseURI, attribute);
                        } else {
                            arrayListReturn.add(attribute);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void format(String baseURI, String attribute) {
        String firstCut = baseURI.substring(0, baseURI.lastIndexOf("/"));
        String lastCut = firstCut.substring(firstCut.lastIndexOf("/") + 1);
        addValues(lastCut, attribute);

    }

    private void addValues(String key, String value) {  //duplicate key hashmap
        ArrayList tempList = null;
        if (hashMap.containsKey(key)) {
            tempList = hashMap.get(key);
            if (tempList == null)
                tempList = new ArrayList();
            tempList.add(value);
        } else {
            tempList = new ArrayList();
            tempList.add(value);
        }
        hashMap.put(key, tempList);
    }
//    public void show(){
//        Iterator it = hashMap.keySet().iterator();
//        ArrayList tempList;
//
//        while (it.hasNext()) {
//            String key = it.next().toString();
//            tempList = hashMap.get(key);
//            if (tempList != null) {
//                for (Object value: tempList) {
//                    System.out.println("Key : "+key+ " , Value : "+value);
//                }
//            }
//        }
//    }
}
