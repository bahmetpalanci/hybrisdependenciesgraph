package com.bap;

/**
 * Created by bap on 7/17/2017.
 */


import static com.bap.AllConstant.ALL_EXT_ADDRESS;

public class main {


    public static void main(String[] args) {

        AllGraph allGraph = new AllGraph();
        allGraph.initialize(); // Tüm graphlar.

        SelectiveDependencies selectiveAllDependencies = selectiveAllDependencies = new SelectiveDependencies(allGraph.getHashMapRead(), allGraph.getAllExtList(), ALL_EXT_ADDRESS);
        selectiveAllDependencies.dependenciesList(); //tüm extensionları ayrı ayrı oluşturur

//        SelectiveXMLGraph selectiveXMLGraph = new SelectiveXMLGraph(); // Verilen bir xml listesindeki bağımlılıkları bulur.
//        selectiveXMLGraph.initialize();
//        SelectiveDependencies selectiveXMLDependencies = new SelectiveDependencies(allGraph.getHashMapRead(), selectiveXMLGraph.getArrayListRead(),SELECTIVE_SAVE_ADDRESS);
//        selectiveXMLDependencies.dependenciesList();


    }
}
