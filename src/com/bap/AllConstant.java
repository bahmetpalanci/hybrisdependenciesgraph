package com.bap;

/**
 * Created by bap on 7/17/2017.
 */
public abstract class AllConstant {
    public static final String ALL_DIRECTORY = "C:/hybris/HYBRISCOMM6400P_0-70002841/hybris/bin";
    public static final String SELECTIVE_DIRECTORY = "C:/Users/bap/Desktop";
    public static final String ALL_EXT = "requires-extension";
    public static final String ALL_NAME = "name";
    public static final String SELECTIVE_EXT = "extension";
    public static final String SELECTIVE_NAME = "name";
    public static final String ALL_PATTERN = "extensioninfo.{xml}";
    public static final String SELECTIVE_PATTERN = "localextensions.{xml}";
    public static final String SELECTIVE_SAVE_ADDRESS = "C:\\Users\\bap\\Desktop\\GraphFile\\";
    public static final String ALL_EXT_ADDRESS = "C:\\Users\\bap\\Desktop\\AllGraphFile\\";
}
