package com.bap;


import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by bap on 7/17/2017.
 */
public class SelectiveDependencies {

    HashMap<String, ArrayList> allGraph;
    ArrayList<String> selectiveGraph;
    ArrayList repeatNodeList = new ArrayList();
    ArrayList<String> contentList = new ArrayList();
    String saveAddress;

    //    PrintWriter out = new PrintWriter("C:\\Users\\bap\\Desktop\\kayıt\\kayıt.doc");
    public SelectiveDependencies(HashMap<String, ArrayList> allGraph, ArrayList<String> selectiveGraph, String saveAddress) {
        this.allGraph = allGraph;
        this.selectiveGraph = selectiveGraph;
        this.saveAddress = saveAddress;
    }

    public void dependenciesList() {


        for (int i = 0; i < selectiveGraph.size(); i++) {

            String passValue = selectiveGraph.get(i);
            recursion(passValue);
            try (PrintWriter out = new PrintWriter(saveAddress + passValue + ".dot")) {
                out.println("digraph g{");
                for (String s : contentList) {
                    out.println(s);
                }
                out.println("}");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            System.out.println("dot -Tpng " + passValue + ".dot -o " + passValue + ".png  &"); // komut satırına 120'şer şekilde elle giricez

/*komut satırından otomatik çalıştırmayı bu programın komutları için kullanamıyoruz.

            ProcessBuilder pb = new ProcessBuilder(
                    "dot", "-Tpng acceleratorcms.dot" , "-o acceleratorcms.png");
            pb.directory(new File("C:\\Users\\bap\\Desktop\\AllGraphFile\\"));
            pb. redirectErrorStream(true);

            try {
                pb.start();
            } catch (IOException e) {
                e.printStackTrace();
            }*/

            this.repeatNodeList.clear();
            this.contentList.clear();

        }

    }


    public void recursion(String passValue) {
        Iterator it = allGraph.keySet().iterator();
        ArrayList childList = null;

        while (it.hasNext()) {
            String key = it.next().toString();
            if (key.equals(passValue)) {
                if (!repeatNodeList.contains(passValue)) {
                    childList = allGraph.get(key);
                    repeatNodeList.add(key);
                    if (childList != null) {
                        for (Object value : childList) {
                            contentList.add(key + "->" + value + ";");
                            recursion((String) value);
                        }
                    }
                }
            }
        }
    }
}



